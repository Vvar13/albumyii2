<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use app\models\Photo;

/**
 * Class Album
 * @package app\models
 */
class Album extends ActiveRecord
{

    public function rules()
    {
        return [
            [
                [
                    'name',
                    'description',
                    'photographer',
                ],
                'required'
            ],
            ['email', 'email'],
        ];
    }

    /**
     * @param null $id
     * @return array
     */
    static function getIdName($id = null)
    {
        $albums = $id ? self::findOne($id) : self::find()->all();
        $result = [];
        if (is_array($albums)) {
            foreach ($albums as $album) {
                $result[$album->id] = $album->name;
            }
        } elseif ($albums instanceof self) {
            $result[$albums->id] = $albums->name;
        }
        return $result;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getPhotos()
    {
        return Photo::find()->where(['albumId' => $this->id])->orderBy('createDateTime DESC')->all();
    }

    /**
     * @return int
     */
    public function getCountPhotos()
    {
        return count($this->getPhotos());
    }

    /**
     * @return int
     */
    public function getLastPhoto()
    {
        $photo = null;
        if ($this->getPhotos()) {
            $photo = $this->getPhotos()[0];
        }
        return $photo;
    }
}
