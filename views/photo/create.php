<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Photo;
use app\models\Album;


/** @var Photo $photo */
/** @var Album $album */

?>
<div class="site-index">
    <h1>Добавить фото</h1>
    <div class="form-group-sm">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

        <?php echo $form->field($photo, 'albumId')->label('альбом')->dropDownList($options); ?>

        <?= $form->field($photo, 'title')->label('Заголовок фотографии') ?>

        <?= $form->field($photo, 'address')->label('Адрес фотосъемки ') ?>

        <?= $form->field($model, 'imageFile')->fileInput()->label('Загрузите файл') ?>

        <br/>

        <div class="form-group">
            <?= Html::submitButton('создать', ['class' => 'btn btn-primary']) ?>
            <a class="btn bg-primary" href="<?php echo \Yii::$app->urlManager->createUrl(['album']); ?>">вернуться</a>
        </div>
    <?php ActiveForm::end() ?>
    </div>
</div>