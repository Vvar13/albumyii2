<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'Просмотр фото';
?>

<div class="site-index">
    <h1>Просмотреть фото</h1>
    <br />
    <div class="form-group-sm">

        название: <?= Html::encode("{$photo->title}") ?>
        адрес: <?= Html::encode("{$photo->address}") ?>
        дата создания: <?= Html::encode("{$photo->createDateTime}") ?>
        <div class="img-thumbnail">
            <?php echo $img = Html::img('/uploads/' . $photo->imageFile, ['class' => 'img-thumbnail']);?>
        </div>

        <div class="form-group">
            <a class="btn bg-primary" href="<?php echo \Yii::$app->urlManager->createUrl(['album']); ?>">вернуться</a>
        </div>

    </div>
</div>