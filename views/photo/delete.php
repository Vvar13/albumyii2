<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Photo;
use app\models\Album;


/** @var Photo $photo */
/** @var Album $album */

?>
<div class="site-index">
    <h1>Удаление фото</h1>
    <div class="form-group-sm">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <p>Вы действительно хотите удалить фото<span class="text-primary"><?php echo $photo->title; ?></span></p>

            <?php  echo $form->field($photo, 'id')->hiddenInput()->label(false); ?>

            <div class="form-group">
                <?= Html::submitButton('удалить', ['class' => 'btn btn-primary']) ?>
                <a class="btn bg-primary" href="<?php echo \Yii::$app->urlManager->createUrl(['album/view', 'id' => $photo->albumId]); ?>">вернуться</a>
            </div>
        <?php ActiveForm::end() ?>
    </div>
</div>