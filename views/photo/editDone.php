<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Photo;


/** @var  Photo $photo */

?>
<div class="site-index">
    <h1>Изменение фото</h1>

        <p>Фото <span class="text-primary"><?php echo $photo->title; ?></span> успешно добаленно в альбом.</p>
        <div class="form-group">
            <a class="btn bg-primary" href="<?php echo \Yii::$app->urlManager->createUrl(['album/view', 'id' => $photo->albumId]); ?>">к альбому</a>
        </div>
</div>