<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Photo;
use app\models\Album;


/** @var Photo $photo */
/** @var Album $album */

?>
<div class="site-index">
    <h1>Изменение фото</h1>
    <div class="form-group-sm">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

        <?= $form->field($photo, 'title')->label('Заголовок фотографии') ?>

        <?= $form->field($photo, 'address')->label('Адрес фотосъемки ') ?>

        <div class="img-thumbnail">
            <?php echo $img = Html::img('/uploads/' . $photo->imageFile, ['class' => 'img-thumbnail']);?>
        </div>

        <div class="form-group">
            <?= Html::submitButton('изменить', ['class' => 'btn btn-primary']) ?>
            <a class="btn bg-primary" href="<?php echo \Yii::$app->urlManager->createUrl(['album']); ?>">вернуться</a>
        </div>
    <?php ActiveForm::end() ?>
    </div>
</div>