<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Создание альбома';
?>

<div class="site-index">
    <h1>Создание альбома</h1>
    <br />
    Альбом <span class="text-primary"><?php echo $model->name; ?></span> успешно создан.
    <br />
    <br />
    <div class="form-group">
        <a class="btn bg-primary" href="<?php echo \Yii::$app->urlManager->createUrl(['album\create']); ?>">создать еще</a>
        <a class="btn bg-primary" href="<?php echo \Yii::$app->urlManager->createUrl(['album']); ?>">к списку альбомов</a>
    </div>

    </div>
</div>