<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'Изменение альбома';
?>

<div class="site-index">
    <h1>Изменение альбома</h1>
    <br />
    <div class="form-group-sm">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->label('Название альбома') ?>

        <?= $form->field($model, 'description')->label('Описание альбома') ?>

        <?= $form->field($model, 'photographer')->label('Имя фотографа') ?>

        <?= $form->field($model, 'email')->label('Адрес эл. почты') ?>

        <?= $form->field($model, 'phone')->label('Контактный телефон') ?>

        <div class="form-group">
            <?= Html::submitButton('изменить', ['class' => 'btn btn-primary']) ?>
            <a class="btn bg-primary" href="<?php echo \Yii::$app->urlManager->createUrl(['album']); ?>">вернуться</a>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>