<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Изменение альбома';
?>

<div class="site-index">
    <h1>Изменение альбома</h1>
    <br />
    Альбом <span class="text-primary"><?php echo $model->name; ?></span> успешно изменен.
    <br />
    <br />
    <div class="form-group">
        <a class="btn bg-primary" href="<?php echo \Yii::$app->urlManager->createUrl(['album']); ?>">к списку альбомов</a>
    </div>

    </div>
</div>