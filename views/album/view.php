<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Photo;
use app\models\Album;

/** @var Photo $photo */

$this->title = 'Создание альбома';
?>

<div class="site-index">
    <h1>Просмотреть альбом</h1>
    <br />
    <div class="form-group-sm">

        <ul>
            <li><label>Название альбома: </label>: <?= Html::encode($model->name) ?></li>
            <li><label>Описание альбома: </label>: <?= Html::encode($model->description) ?></li>
            <li><label>Имя фотографа: </label>: <?= Html::encode($model->photographer) ?></li>
            <li><label>Адрес эл. почты: </label>: <?= Html::encode($model->email) ?></li>
            <li><label>Контактный телефон: </label>: <?= Html::encode($model->phone) ?></li>
        </ul>


        <div class="form-group">
            <a class="btn bg-success" href="<?php echo \Yii::$app->urlManager->createUrl(['photo/create', 'albumId' => $model->id]); ?>">Добавить фото</a>
            <a class="btn bg-primary" href="<?php echo \Yii::$app->urlManager->createUrl(['album']); ?>">вернуться</a>
        </div>
    </div>

    <?php
    /** @var Album $model */
    $photos = $model->getPhotos();

    if (count($photos) > 0) { ?>
        <div class="greed">
            <table class="table">
                <tr>
                    <td>Фотографя</td>
                    <td>Действия</td>
                </tr>
                <?php foreach ($photos as $photo): ?>
                    <tr>
                        <td>
                            название: <?= Html::encode("{$photo->title}") ?>
                            адрес: <?= Html::encode("{$photo->address}") ?>
                            дата создания: <?= Html::encode("{$photo->createDateTime}") ?>
                            <div class="img-thumbnail">
                            <?php echo $img = Html::img('/uploads/' . $photo->imageFile, ['class' => 'img-thumbnail']);?>
                            </div>
                        </td>
                        <td>
                            <p><a class="btn" href="<?php echo \Yii::$app->urlManager->createUrl(["photo/view", 'id' => $photo->id]); ?>">просмотреть</a></p>
                            <p><a class="btn" href="<?php echo \Yii::$app->urlManager->createUrl(["photo/edit", 'id' => $photo->id]); ?>">редактировать</a></p>
                            <p><a class="btn" href="<?php echo \Yii::$app->urlManager->createUrl(['photo/delete', 'id' => $photo->id]); ?>">удалить</a></p>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    <?php } ?>
</div>