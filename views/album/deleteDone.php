<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Photo;


/** @var  Photo $photo */

?>
<div class="site-index">
    <h1>Удаление альбома</h1>

        <p>Альбом успешно удалено.</p>
        <div class="form-group">
            <a class="btn bg-primary" href="<?php echo \Yii::$app->urlManager->createUrl(['album']); ?>">к списку альбомов</a>
        </div>
</div>