<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\LinkPager;
use app\models\Album;

/** @var Album $album */

$this->title = 'Альбомы';
?>

<div class="site-index">
    <h1>Список альбомов</h1>
    <br />
    <div class="form-group">
        <a class="btn bg-info" href="<?php echo \Yii::$app->urlManager->createUrl(['album/create']); ?>">добавить новый альбом</a>
        <a class="btn bg-success" href="<?php echo \Yii::$app->urlManager->createUrl(['photo/create']); ?>">добавить фото</a>
    </div>

    <div class="greed">
        <table class="table">
            <tr>
                <td>Название</td>
                <td>Фотографий в альбоме</td>
                <td>Последнее добавленное фото</td>
                <td>Действия</td>
            </tr>
            <?php foreach ($model as $album): ?>
            <tr>
                <td>
                    <?= Html::encode("{$album->name}") ?>
                </td>
                <td>
                    <?= Html::encode("{$album->getCountPhotos()}") ?>
                </td>
                <td>
                    <?php $photo = $album->getLastPhoto();
                    if ($photo) {
                        echo "дата создания:". Html::encode("{$photo->createDateTime}");
                        echo "<br/>";
                        echo Html::img('/uploads/' . $photo->imageFile, ['class' => 'img-thumbnail', 'width'=>"100", 'height'=>"100"]);
                    }
                    ?>
                </td>
                <td>
                    <p><a class="btn" href="<?php echo \Yii::$app->urlManager->createUrl(["album/view", 'id' => $album->id]); ?>">просмотреть</a></p>
                    <p><a class="btn" href="<?php echo \Yii::$app->urlManager->createUrl(['album/edit', 'id' => $album->id]); ?>">редактировать</a></p>
                    <p><a class="btn" href="<?php echo \Yii::$app->urlManager->createUrl(['album/delete', 'id' => $album->id]); ?>">удалить</a></p>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?= LinkPager::widget(['pagination' => $pagination]) ?>
    </div>
</div>
