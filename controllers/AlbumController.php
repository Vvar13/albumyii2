<?php

namespace app\controllers;

use app\models\Photo;
use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Album as ModelAlbum;

/**
 * Class TestController
 * @package app\controllers
 */
class AlbumController extends Controller
{
    /**
     * Таблица альбомов
     * @return string
     */
    public function actionIndex()
    {

        $query = ModelAlbum::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $modelAlbum = $query->orderBy('name')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();


        return $this->render('index', [
            'model' => $modelAlbum,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $view = 'create';

        $model = new modelAlbum();

        if ($model->load($request->post()) && $model->validate()) {
            $model->createDateTime = (new \DateTime())->format('Y:m:d h:i:s');
            $model->editDateTime = (new \DateTime())->format('Y:m:d h:i:s');
            $model->save();
            $view = 'createDone';
        }

        return $this->render($view, [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $view = 'delete';

        /** @var ModelAlbum $album */
        $album = ModelAlbum::findOne($id);

        if ($album->load($request->post()) && $album->validate()) {
            $photoAlbum = $request->post();
            if ($photoAlbum['Album']['id'] == $id) {
                /** @var Photo $photo */
                foreach($album->getPhotos() as $photo) {
                    $photo->delete();
                }
                $album->delete();
                $view = 'deleteDone';
            } else {
                throw new \Exception('Не верный параметр');
            }
        }

        return $this->render($view, [
            'album' => $album
        ]);
    }

    /**
     * @return string
     */
    public function actionEdit($id)
    {
        $request = Yii::$app->request;
        $view = 'edit';

        /** @var ModelAlbum $model */
        $model = ModelAlbum::findOne($id);

        if ($model->load($request->post()) && $model->validate()) {
            $model->editDateTime = (new \DateTime())->format('Y:m:d h:i:s');
            $model->save();
            $view = 'editDone';
        }

        return $this->render($view, [
            'model' => $model
        ]);
    }


    public function actionView($id)
    {
        /** @var ModelAlbum $model */
        $model = ModelAlbum::findOne($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }
}