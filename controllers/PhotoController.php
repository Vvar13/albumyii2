<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\UploadForm;
use app\models\Photo;
use app\models\Album;
use yii\web\UploadedFile;

/**
 * Class TestController
 * @package app\controllers
 */
class PhotoController extends Controller
{

    public function actionIndex()
    {
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $options = Album::getIdName($request->get('albumId'));
        $uploadForm = new UploadForm();
        $photo = new Photo();
        $view = 'create';

        if (Yii::$app->request->isPost) {
            $uploadForm->imageFile = UploadedFile::getInstance($uploadForm, 'imageFile');

            if ($uploadForm->upload()) {
                $photo->imageFile = $uploadForm->imageFile;
                $photoPost = $request->post('Photo');
                $photo->title = $photoPost['title'];
                $photo->address = $photoPost['address'];
                $photo->albumId =  $photoPost['albumId'];
                $photo->createDateTime = (new \DateTime())->format('Y:m:d h:i:s');
                $photo->editDateTime = (new \DateTime())->format('Y:m:d h:i:s');
                $photo->save();
                $view = 'createDone';
            }
        }
        return $this->render($view, [
            'model' => $uploadForm,
            'photo' => $photo,
            'options' => $options,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionEdit($id)
    {
        $request = Yii::$app->request;
        $options = Album::getIdName($request->get('albumId'));

        $photo = Photo::findOne($id);
        $view = 'edit';

        if (Yii::$app->request->isPost) {
            $photoPost = $request->post('Photo');

            $photo->title = $photoPost['title'];
            $photo->address = $photoPost['address'];
            $photo->editDateTime = (new \DateTime())->format('Y:m:d h:i:s');
            $photo->save();

            $view = 'editDone';
        }
        return $this->render($view, [
            'photo' => $photo,
            'options' => $options,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;

        $photo = Photo::findOne($id);
        $view = 'delete';

        if (Yii::$app->request->isPost) {
            $photoPost = $request->post();
            if ($photoPost['Photo']['id'] == $id) {
                $photo->delete();
                $view = 'deleteDone';
            } else {
                throw new \Exception('Не верный параметр');
            }
        }

        return $this->render($view, [
            'photo' => $photo,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $photo = Photo::findOne($id);
        return $this->render('view', [
            'photo' => $photo,
        ]);
    }
}